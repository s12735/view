<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%-- <%@ include file="shared/bootstrap.html" %> --%>
<html>
<head>
    <title><%  %></title>
    <link rel="stylesheet" type="text/css" href="mycss.css" media="screen" />
</head>
<body>
<!-- navbar -->
<%@include file="navBar.html"%>
<!-- body -->
<div id="container" style="width: 800px">
	<form action="#" method="post">
	<div id="editRight"> 
		<img src alt="Profile photo" width="150" height="165"/>
		<br/>
		<label for="about">Opis:</label><br/>
		<textarea id="about" name="about" rows="8" cols="10" style="width:100%"></textarea>
	</div>
	<div id="editLeft">
		<label for="photo">Zdjęcie: </label>
		<input type="file" id="photo" name="photo"  placeholder="Lokalizacja zdjęcia" required/>
		<br/><br/>
		<label for="age">Wiek: </label>
		<input type="number" id="age" name="age" />
		<br/><br/>
		<label for="height">Wzrost: </label>
        <input type="number" id="height" name="height" min="50" max="250" required/>
        <br/><br/>
		<label for="eyeColor">Kolor oczu: </label>
        <input type="text" id="eyeColor" name="eyeColor" required/>
        <br/><br/>
		<label for="hairColor">Kolor włosów: </label>
        <input type="text" id="hairColor" name="hairColor" required/> 
        <br/><br/>
		<label for="house">Zamieszkanie: </label>
        <input type="text" id="house" name="house" required/>
        <br/><br/>
		<label for="education">Wyksztacenie : </label>
        <select id="education" name="education" required>
        	 <option value="podstawowe">podstawowe</option>
             <option value="srednie">średnie</option>
             <option value="wyzsze">Wyższe</option>
             
        </select>
	</div>
	<input type="submit" name="submit" value="Save" />
	</form>
</div>	
	
 
        
        
</body>
</html>