<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title><%  %></title>
</head>
<body>
<!-- navbar -->
<%@include file="navBar.html"%>
<div class="container">
<!-- search -->
    <div class="col-md-4">
        <form class="form-horizontal">
            <fieldset>
                <div class="edit.left">
                    <label for="ageFrom">Wiek od: </label>
                    <input type="number" max="120" min="18" value="18" id="ageFrom" name="ageFrom" step="1"/>
                </div>
                <div class="col-md-6">
                    <label for="ageFrom">do: </label>
                    <input type="number" max="120" min="18" value="30" id="ageTo" name="ageTo" step="1"/>
                </div>
                <div class="col-md-12">
                    <label for="school">Wykształcenie: </label>
                    <select name="school" id="school">
                        <option value="Hight">Wyższe</option>
                        <option value="Mid">Srednie</option>
                        <option value="Basic">Podstawowe</option>
                    </select>
                </div>
            </fieldset>
        </form>
    </div>
<!-- results -->
    <div class="container">
        <!-- result -->
        <div class="col-md-8">
            <fieldset>
                <legend></legend>
                <div class="edit.right">
                    <img src="" class="img-rounded" alt="Profile Photo"
                    width="100" height="100">
                </div>
                <div class="col-md-*">
                    <label class="col-sm-1"for="about">Opis</label>
                    <textarea name="about" id="about" readonly rows="5" cols="20"></textarea>
                </div>
            </fieldset>
        </div>
    </div>
</div>
</body>
</html>